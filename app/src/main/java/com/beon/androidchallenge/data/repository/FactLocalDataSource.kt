package com.beon.androidchallenge.data.repository

import android.content.SharedPreferences
import com.beon.androidchallenge.domain.model.Fact
import com.google.gson.Gson

class FactLocalDataSource(
    private val sharedPreferences: SharedPreferences,
    private val gson: Gson,
) {

    fun saveFact(id: String, fact: Fact) {
        val serialized: String = gson.toJson(fact)
        sharedPreferences.edit().putString(id, serialized).apply()
    }

    fun loadFact(id: String): Fact? {
        val serialized = sharedPreferences.getString(id, null)
        return gson.fromJson<Fact>(serialized, Fact::class.java)
    }
}