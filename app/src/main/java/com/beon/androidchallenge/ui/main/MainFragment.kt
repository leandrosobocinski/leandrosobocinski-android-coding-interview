package com.beon.androidchallenge.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.beon.androidchallenge.R
import com.beon.androidchallenge.databinding.MainFragmentBinding

class MainFragment : Fragment() {

    companion object {
        private const val DEBOUNCE_TIME = 2000L
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel
    private lateinit var binding: MainFragmentBinding
    private var previousRunnable: Runnable? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        initViews()
        initObservers()
    }


    private fun cancelPendingSearch() {
        previousRunnable?.let {
            binding.numberEditText.handler.removeCallbacks(it)
            previousRunnable = null
        }
    }
    private fun setupDebounce() {
        binding.numberEditText.addTextChangedListener {
            cancelPendingSearch()

            val runnable = Runnable {
                viewModel.searchNumberFact(it.toString())
                previousRunnable = null
            }
            binding.numberEditText.handler.postDelayed(runnable, DEBOUNCE_TIME)
            previousRunnable = runnable
        }
    }

    private fun initViews() {
        binding.run {
            setupDebounce()

            numberEditText.setOnEditorActionListener { textView, actionId, keyEvent ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    cancelPendingSearch()
                    viewModel.searchNumberFact(textView.text.toString())
                    return@setOnEditorActionListener true
                } else {
                    return@setOnEditorActionListener false
                }
            }

            retryButton.setOnClickListener {
                cancelPendingSearch()
                viewModel.searchNumberFact(numberEditText.text.toString())
            }

            numberEditText.requestFocus()
        }
    }

    private fun showEmpty() {
        binding.loadingProgressBar.visibility = View.GONE
        binding.factTextView.visibility = View.VISIBLE
        binding.retryButton.visibility = View.GONE

        binding.factTextView.setText(R.string.instructions)
    }

    private fun showLoading() {
        binding.loadingProgressBar.visibility = View.VISIBLE
        binding.factTextView.visibility = View.GONE
        binding.retryButton.visibility = View.GONE
    }

    private fun showContent(message: String?) {
        binding.loadingProgressBar.visibility = View.GONE
        binding.factTextView.visibility = View.VISIBLE
        binding.retryButton.visibility = View.GONE

        binding.factTextView.text = message
    }

    private fun showError() {
        binding.loadingProgressBar.visibility = View.GONE
        binding.factTextView.visibility = View.VISIBLE
        binding.retryButton.visibility = View.VISIBLE

        binding.factTextView.setText(R.string.error_message)
    }

    private fun initObservers() {
        viewModel.currentFact.observeForever { pageState ->
            when (pageState) {
                PageState.Empty -> showEmpty()
                is PageState.Content -> showContent(pageState.fact.text)
                PageState.Error -> showError()
                PageState.Loading -> showLoading()
            }
        }
    }
}