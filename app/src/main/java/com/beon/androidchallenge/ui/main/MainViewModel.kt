package com.beon.androidchallenge.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.beon.androidchallenge.data.repository.FactRepository
import com.beon.androidchallenge.domain.model.Fact
import com.beon.androidchallenge.ui.main.PageState.*

sealed class PageState {
    object Empty: PageState()
    object Loading: PageState()
    object Error: PageState()
    data class Content(val fact: Fact): PageState()
}

class MainViewModel : ViewModel() {

    val currentFact = MutableLiveData<PageState>(Empty)

    fun searchNumberFact(number: String) {
        if (number.isEmpty()) {
            currentFact.postValue(Empty)
            return
        }

        currentFact.postValue(Loading)
        FactRepository.getInstance().getFactForNumber(number, object : FactRepository.FactRepositoryCallback<Fact> {
            override fun onResponse(response: Fact) {
                currentFact.postValue(Content(response))
            }

            override fun onError() {
                currentFact.postValue(Error)
            }
        })
    }
}